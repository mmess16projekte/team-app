/* global Tactic, fabric,EventPublisher*/
Tactic.TacticView = function(options){
    var that =new EventPublisher(),
        field,
        stepLabel,
        contextCanvas,
        canvasContainer,
        menu;

    function init(){
        //clone object of field to show in edit menu
        var clone, fieldBackground=options.fieldBackground, img;
        var x = document.createElement("IMG");
        x.setAttribute("src", fieldBackground);
        contextCanvas=options.contextCanvas;
        canvasContainer=options.canvasContainer;
        menu=options.menu;
        field=options.field;
        stepLabel=options.stepLabel;
        img = new fabric.Image(x);
        canvasContainer.style.width=img.width;
        field.setWidth(img.width);
        field.setHeight(img.height);
        field.setBackgroundImage(img, field.renderAll.bind(field), {
        // Needed to position backgroundImage at 0/0
            originX: "left",
            originY: "top"
        });
        //show edit menu
        field.on({
            "object:selected": function(e) {
                if (e.target&&e.target.item!=undefined) {
                    menu.style.display = "block";
                    clone = fabric.util.object.clone(e.target);
                    clone.set({left: 10,top: 10});
                    contextCanvas.add(clone);
                    that.notifyAll("menuActivated");
                }
            },
            "selection:cleared": function(e) {
                if (e.target) {
                    closeMenu();
                }
            },
            "object:moved": function(e) {
                e.target.opacity = 0.5;
            },
            "object:modified": function(e) {
                e.target.opacity = 1;
            }
        });
        //this.__canvases.push(field);
        return that;
    }
    function closeMenu(){
        menu.style.display = "none";
    }
    //update number of player object
    function updateObject(){
        var object=field.getActiveObject();
        var num =document.querySelector("#num-input").value;
        closeMenu();
        if(object.item==undefined){
            return;
        }
        object.item(1).set({
            text: num+""
        });
        field.renderAll();
    }

    function updateStepLabel(step){
        stepLabel.innerHTML="Schritt "+step;
    }

    function getObjects(){
        return field.getObjects();
    }

    function findObjectById(id,object) {
        return object.id === id;
    }
//reset objects to the position they had in the step before
    function resetObjects(objects){
        var i, allObjects=getObjects(), currObject,getObj;
        if(objects==null){
            return;
        }
        for(i=0;i<objects.length;i++){
            getObj = findObjectById.bind(null, objects[i].id);
            currObject=allObjects.find(getObj);
            if(currObject!=undefined){
                currObject.set({left:objects[i].left});
                currObject.set({top:objects[i].top});
                currObject.setCoords();
                field.renderAll();
            }
        }
    }

//move object based on the given top left offset
    function showAnimation(id,left,top){
        var getImg = findObjectById.bind(null, id);
        var img=field.getObjects().find(getImg);
        img.animate("left",left,{
            onChange:field.renderAll.bind(field)
        });
        img.animate("top",top,{
            onChange:field.renderAll.bind(field)
        });

    }
    //add object. if a player object is chosen, the object is a group composed of image and text
    function addImage(img,coordinates,id){
        var imgNum,group;
        var newImage = new fabric.Image(img, {
            width: img.width,
            height: img.height,
            originX: "center",
            originY: "center",
            // Set the center of the new object based on the event coordinates relative
            // to the canvas container.
            left: coordinates.x,
            top: coordinates.y,
            id:id
        });
        if(img.parentNode.id=="balls"){
            newImage.hasControls = newImage.hasBorders = false;
            field.add(newImage);
            return;
        }
        imgNum = new fabric.Text(id, {
            fontSize: 15,
            originX: "center",
            originY: "center",
            left: coordinates.x,
            top: coordinates.y
        });
        group = new fabric.Group([ newImage, imgNum ],{
            id:id
        });
        group.hasControls = group.hasBorders = false;

        field.add(group);
        field.hoverCursor = "pointer";

    }
    that.init=init;
    that.addImage=addImage;
    that.showAnimation=showAnimation;
    that.getObjects=getObjects;
    that.resetObjects=resetObjects;
    that.updateStepLabel=updateStepLabel;
    that.updateObject=updateObject;
    return that;
};
