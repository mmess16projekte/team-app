/* global require, module, process */
var request = require("request");
var apiOptions = {
    server : "http://localhost:3000"
};
if (process.env.NODE_ENV === "production") {
    apiOptions.server = "https://mme-team-app.herokuapp.com";
}

/* POST "add-games" page */
module.exports.addGame = function (req, res) {
    var requestOptions, path, postdata, type = false;
    var redirect = "/gameslist", failureRedirect = "/addgame?type=training&err=true";
    path = "/api/games";
    if (req.body.type === "tournament") {
        type = true;
        redirect = "/tournament";
        failureRedirect = "/addgame?type=tournament&err=true";
    }
    postdata = {
        location: req.body.location,
        date: req.body.date,
        comment: req.body.comment,
        mode: req.body.mode,
        cost: req.body.cost,
        tournament: type
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "POST",
        json: postdata
    };
    // error handling  einfuegen, loc date comment da etc. + status 400?
    doRequest(req, res, requestOptions, 201, redirect, failureRedirect);
};
/* Post "addComment" page */
module.exports.addComment = function(req, res){
    var requestOptions, path, postdata;
    var gameid = req.params.gameid;
    var redirect = "/gameslist/" + gameid;
    path = "/api/games/" + gameid + "/addcomment";
    postdata = {
        user: req.user.name,
        comment: req.body.comment
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "POST",
        json: postdata
    };
    doRequest(req, res, requestOptions, 201, redirect);
};
/* Post "changeStatus" page */
module.exports.changeStatus = function(req, res){
    var requestOptions, path, postdata, redirect;
    var gameid = req.params.gameid;
    var type = "?type=training";
    if (req.query.tournament === "true") {
        type = "?type=tournament";
    }
    redirect = "/gameslist/" + gameid + type;
    path = "/api/games/" + gameid + "/changestatus";
    postdata = {
        userId: req.user.id,
        agreed: req.body.agreed
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "PUT",
        json: postdata
    };
    doRequest(req, res, requestOptions, 201, redirect);
};
/* Post "changePresent" page */
module.exports.changePresent = function(req, res){
    var requestOptions, path, type, postdata, redirect, present = true;
    var gameid = req.params.gameid;
    path = "/api/games/" + gameid + "/changepresent";
    type = "?type=training";
    if (req.query.tournament === "true") {
        type = "?type=tournament";
    }
    redirect = "/gameslist/" + gameid + type;
    if (req.body.present === "nonpresent") { present = false; }
    postdata = {
        userId: req.query.userid,
        present: present
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "PUT",
        json: postdata
    };
    doRequest(req, res, requestOptions, 201, redirect);
};
/* Post "changeTeam" page */
module.exports.changeTeam = function(req, res) {
    var requestOptions, path, redirect;
    var gameid = req.params.gameid;
    var type = "?type=training";
    if (req.body.type === "tournament") {
        type = "?type=tournament";
    }
    redirect = "/gameslist/" + gameid + type;
    if (req.body.type === "tournament") {
        type = "?type=tournament";
    }
    path = "/api/games/" + gameid + "/changeteam";
    requestOptions = {
        url: apiOptions.server + path,
        method: "PUT",
        json: req.body
    };
    doRequest(req, res, requestOptions, 201, redirect);
};
/* Post "changeOutcome" page */
module.exports.changeOutcome = function(req, res) {
    var postdata, requestOptions, path, gameid = req.params.gameid, won = true;
    var redirect = "/tournament/";
    if (req.body.outcome === "lost") { won = false; }
    path = "/api/games/" + gameid + "/changeoutcome";
    postdata = {
        outcome: won
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "PUT",
        json: postdata
    };
    doRequest(req, res, requestOptions, 201, redirect);
};

var doRequest = function (req, res, requestOptions, status, succesRedirect, failureRedirect) {
    request(
        requestOptions,
        function(err, response, body) {
            if (response.statusCode === status) {
                res.redirect(succesRedirect);
            } else if (response.statusCode === 400 && body.name && body.name === "ValidationError") {
                res.redirect(failureRedirect);
            } else {
                _error(req, res, response.statusCode);
            }
        }
    );
};

var _error = function (req, res, status) {
    res.render("generic-text", {
        title: status + "Error",
        message: "An Error happended"
    });
};