Online Demo at:
https://mme-team-app.herokuapp.com/

Installation Manual:

If you want to install the Team-App Application must have Node.js installed on your machine.

Installation Instructions:

- clone the repository to your local machine
- in a terminal switch to the /app folder of the cloned repository
- install the npm modules using "npm install"
- start the application using "npm start"
- open a browser and navigate to "http://localhost:3000/"
- have fun!