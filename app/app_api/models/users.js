/* global require */
var mongoose = require( "mongoose" );
var crypto = require("crypto");

var statsSchema = new mongoose.Schema({
    attendedTrainings: {
        type: Number,
        "default": 0
    },
    missedTrainings: {
        type: Number,
        "default": 0
    },
    attendedTournaments: {
        type: Number,
        "default": 0
    },
    missedTournaments: {
        type: Number,
        "default": 0
    },
    tournamentsWon: {
        type: Number,
        "default": 0
    },
    tournamentsLost: {
        type: Number,
        "default": 0
    }
});

var userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    location: String,
    trainer: {
        type: Boolean,
        required: true
    },
    stats: statsSchema,
    hash: String,
    salt: String
});

userSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString("hex");
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString("hex");
};

userSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString("hex");
    return this.hash === hash;
};

mongoose.model("User", userSchema);
