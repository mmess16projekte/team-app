/* global require, module, process */
var request = require("request");
var apiOptions = {
    server : "http://localhost:3000"
};
if (process.env.NODE_ENV === "production") {
    apiOptions.server = "https://mme-team-app.herokuapp.com";
}

/* GET "home" page */
module.exports.home = function(req, res){
    res.render("index", {
        title: "Willkommen zur Team-App",
        pageHeader: "Herzlich willkommen",
        message: "Zur Nutzung der Team-App bitte einloggen oder registrieren"
    });
};
/* GET "player" page */
module.exports.player = function (req, res) {
    var user;
    if (req.isAuthenticated()) { user = req.user; }
    res.render("player", {
        navbarOptions: {
            active: "player",
            user: user
        },
        pageHeader: "Spielerübersicht für " + user.name,
        stats: JSON.stringify(user.stats)
    });
};
/* GET "tactic" page */
module.exports.tactics = function (req, res) {
    var path, requestOptions;
    path = "/api/tactics";
    requestOptions = {
        url: apiOptions.server + path,
        method: "GET",
        json: {}
    };
    request(
        requestOptions,
        function (err, response, body) {
            renderTactic(req, res, body);
        }
    );
};
/* GET "ranking" page */
module.exports.ranking = function (req, res) {
    var path, requestOptions;
    path = "/users";
    requestOptions = {
        url: apiOptions.server + path + "?sort=stats.attendedTrainings",
        method: "GET",
        json: {}
    };
    request(
        requestOptions,
        function (err, response, trainingSortBody) {
            requestOptions = {
                url: apiOptions.server + path + "?sort=stats.attendedTournaments",
                method: "GET",
                json: {}
            };
            request(
                requestOptions,
                function (err, response, tournamentSortBody) {
                    renderRanking(req, res, trainingSortBody, tournamentSortBody);
                }
            );
        }
    );
};

var renderRanking = function (req, res, trainingSortBody, tournamentSortBody) {
    var user;
    if (req.isAuthenticated()) { user = req.user; }
    res.render("ranking", {
        navbarOptions: {
            active: "ranking",
            user: user
        },
        title: "Team-App Ranking",
        pageHeader: "Ranking",
        usersTrainingSort: trainingSortBody,
        usersTournamentSort: tournamentSortBody
    });
};

var renderTactic = function (req, res, body) {
    var user;
    if (req.isAuthenticated()) { user = req.user; }
    res.render("tactic", {
        navbarOptions: {
            active: "tactic",
            user: user
        },
        title: "Team-App Taktik erstellen",
        pageHeader: "Taktik erstellen",
        tactics: body
    });
};

/* GET "about" page */
module.exports.about = function(req, res){
    res.render("generic-text", {
        navbarOptions: {
            active: "about"
        },
        title: "About Team-App",
        message: "Die Team-app ist im Rahmen des Kurses Multimedia Engineering entstanden. \nFolgende Komponenten werden in der Team-App benutzt: \nNode.js Express Server der Passport zur Authentifizierung, eine MongoDB als Datenspeicher und Bootstrap als Frontend Framework verwendet.\nAußerdem werden folgende Javascript Libraries verwendet:\n-JQery\n-Chart.js\n-fabric.js\n-underscore.js"
    });
};