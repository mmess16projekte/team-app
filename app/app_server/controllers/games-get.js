/* global require, module, process */
var request = require("request");
var apiOptions = {
    server : "http://localhost:3000"
};
if (process.env.NODE_ENV === "production") {
    apiOptions.server = "https://mme-team-app.herokuapp.com";
}

/* GET "games-list" page */
module.exports.gamesList = function(req, res){
    var path, postdata, requestOptions;
    path = "/api/games";
    postdata = {
        tournament: false
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "GET",
        json: postdata
    };
    request(
        requestOptions,
        function (err, response, body) {
            renderGamesList(req, res, body, false);
        }
    );
};
/* GET "game-detail" page */
module.exports.gameDetail = function(req, res){
    var path, requestOptions, id = req.params.gameid;
    path = "/api/games/" + id;
    requestOptions = {
        url: apiOptions.server + path,
        method: "GET",
        json: {}
    };
    request(
        requestOptions,
        function (err, response, gamesBody) {
            path = "/api/tactics/";
            requestOptions = {
                url: apiOptions.server + path,
                method: "GET",
                json: {}
            };
            request(
                requestOptions,
                function (err, response, tacticsBody) {
                    renderGameDetail(req, res, gamesBody, tacticsBody);
                }
            );
        }
    );
};
/* GET "add-games" page */
module.exports.addGame = function (req, res) {
    renderAddGame(req, res);
};
/* GET "tournamens" page */
module.exports.tournamentsList = function(req, res){
    var path, postdata, requestOptions;
    path = "/api/games";
    postdata = {
        tournament: true
    };
    requestOptions = {
        url: apiOptions.server + path,
        method: "GET",
        json: postdata
    };
    request(
        requestOptions,
        function (err, response, body) {
            renderGamesList(req, res, body, true);
        }
    );
};

var renderGamesList = function(req, res, responseBody, isTournament){
    var message, user, options;
    if (!(responseBody instanceof Array)) {
        message = "API fehler";
        responseBody = [];
    } else {
        if (!responseBody.length) {
            message = "Kein Termin gefunden!";
        }
    }
    if (req.isAuthenticated()) { user = req.user; }
    if (isTournament) {
        options = {
            navbarOptions: {
                active: "tournament-list",
                user: user
            },
            title: "Team-App Turnier Übersicht",
            pageHeader: "Turniere",
            tournament: isTournament,
            games: _modifyDates(responseBody),
            message: message
        }
    } else {
        options = {
            navbarOptions: {
                active: "games-list",
                user: user
            },
            title: "Team-App Trainings Übersicht",
            pageHeader: "Trainingsübersicht",
            tournament: isTournament,
            games: _modifyDates(responseBody),
            message: message
        }
    }
    res.render("games-list", options);
};

var renderGameDetail = function (req, res, gamesBody, tacticsBody) {
    var user, isTournament = false;
    if (req.isAuthenticated()) { user = req.user; }
    if (req.query.type === "tournament") { isTournament = true; }
    res.render("game-detail", {
        navbarOptions: {
            user: user
        },
        _id: gamesBody._id,
        location: gamesBody.location,
        date: _dateAsString(gamesBody.date),
        tournament: isTournament,
        comment: gamesBody.comment,
        mode: gamesBody.mode,
        cost: gamesBody.cost,
        agreedUser: gamesBody.agreedUser,
        disagreedUser: gamesBody.disagreedUser,
        feedback: _modifyDates(gamesBody.feedback),
        team1: gamesBody.team1,
        team2: gamesBody.team2,
        won: gamesBody.won,
        tactics: tacticsBody
    });
};

var renderAddGame = function (req, res) {
    var user, active = "games-list", pageHeader = "Training erstellen";
    if (req.isAuthenticated()) { user = req.user; }
    if (req.query.type === "tournament") {
        active = "tournament-list";
        pageHeader = "Turnier erstellen";
    }
    res.render("add-game-form", {
        navbarOptions: {
            active: active,
            user: user
        },
        title: "Team-App Training erstellen",
        pageHeader: pageHeader,
        error: req.query.err
    });
};

var _modifyDates = function (games) {
    var i, dateFieldName = "date", results = [];
    if (games.length && games[0]["createdOn"]) {
        dateFieldName = "createdOn";
    }
    for (i = 0; i < games.length; i++) {
        games[i][dateFieldName] = _dateAsString(games[i][dateFieldName]);
        results.push(games[i]);
    }
    return results;
};

var _dateAsString = function (dateString) {
    var date = new Date(dateString);
    var day = date.getDate(), month = date.getMonth()+1, year = date.getFullYear();
    return day + "." + month + "." + year;
};