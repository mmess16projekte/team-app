/*global require, module */
var express = require("express");
var router = express.Router();
var ctrlOthers = require("../controllers/others");
var ctrlGames = require("../controllers/games-get");
var ctrlGamesPost = require("../controllers/games-post");
var ctrlAuth = require("../controllers/auth");

var isLoggedIn = function (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.redirect("/");
};

/* ---Games Pages--- */
/* Player Page */
router.get("/player", isLoggedIn, ctrlOthers.player);
/* Training Pages */
router.get("/gameslist", isLoggedIn, ctrlGames.gamesList);
router.get("/gameslist/:gameid", isLoggedIn, ctrlGames.gameDetail);
router.post("/gameslist/:gameid/addcomment", isLoggedIn, ctrlGamesPost.addComment);
router.post("/gameslist/:gameid/changestatus", isLoggedIn, ctrlGamesPost.changeStatus);
router.post("/gameslist/:gameid/changepresent", isLoggedIn, ctrlGamesPost.changePresent);
router.post("/gameslist/:gameid/changeteam", isLoggedIn, ctrlGamesPost.changeTeam);
router.post("/gameslist/:gameid/changeoutcome", isLoggedIn, ctrlGamesPost.changeOutcome);
router.get("/addgame", isLoggedIn, ctrlGames.addGame);
router.post("/addgame", isLoggedIn, ctrlGamesPost.addGame);
/* Tournament Pages */
router.get("/tournament", isLoggedIn, ctrlGames.tournamentsList);
/* Tactic Pages */
router.get("/tactic", isLoggedIn, ctrlOthers.tactics);
/* Ranking Pages */
router.get("/ranking", isLoggedIn, ctrlOthers.ranking);

/* Other Pages */
router.get("/", ctrlOthers.home);
router.get("/login", ctrlAuth.login);
router.post("/login", ctrlAuth.doLogin);
router.get("/signup", ctrlAuth.signup);
router.post("/signup", ctrlAuth.doSignup);
router.get("/logout", ctrlAuth.logout);
router.get("/users", ctrlAuth.userList);
router.get("/about", ctrlOthers.about);


module.exports = router;
