/* global require, module */
var mongoose = require("mongoose");
var Game = mongoose.model("Game");
var User = mongoose.model("User");

module.exports.gamesList = function (req, res) {
    Game.find({"tournament": req.body.tournament})
        .select("location date comment mode cost")
        .sort({date: -1})
        .exec(function (err, games) {
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                sendJSONresponse(res, 200, games);
            }
        }
    );
};

module.exports.gamesCreate = function (req, res) {
    Game.create({
        location: req.body.location,
        date: req.body.date,
        comment: req.body.comment,
        tournament: req.body.tournament,
        mode: req.body.mode,
        cost: req.body.cost
    }, function (err, game) {
        if (err) {
            sendJSONresponse(res, 400, err);
        } else {
            sendJSONresponse(res, 201, game);
        }
    });
};

module.exports.gamesReadOne = function (req, res) {
    var id = req.params.gameid;
    if (id) {
        Game
            .findById(id)
            .populate("agreedUser.user")
            .populate("disagreedUser")
            .populate("team1.user")
            .populate("team1.tactic")
            .populate("team2.user")
            .populate("team2.tactic")
            .exec(function (err, game) {
                if (!game) {
                    sendJSONresponse(res, 404, {
                        message: "gameid not found"
                    });
                    return;
                } else if (err) {
                    sendJSONresponse(res, 404, err);
                    return;
                }
                sendJSONresponse(res, 200, game);
            });
    } else {
        sendJSONresponse(res, 404, {
            message: "no gameid in request"
        });
    }
};

module.exports.gameChangeStatus = function (req, res) {
    var id = req.params.gameid, userId = req.body.userId;
    var pushStatement, pullStatement;
    if (req.body.agreed === "agreed") {
        pushStatement = {agreedUser: { $each:[{user: userId}]}};
    } else {
        pushStatement = {disagreedUser: userId};
    }
    pullStatement = {
        $pull: {
            agreedUser: {user: userId},
            disagreedUser: userId
        }
    };
    Game.update(
        {_id: id},
        pullStatement,
        {safe: true},
        function (err, game) {
            if (err) {
                sendJSONresponse(res, 400, err);
            } else {
                Game.findByIdAndUpdate(
                    id,
                    {$push: pushStatement},
                    {safe: true, upsert: true},
                    function (err, game) {
                        if (err) {
                            sendJSONresponse(res, 400, err);
                        } else {
                            sendJSONresponse(res, 201, game);
                        }
                    }
                );
            }
        }
    );
};

module.exports.gameChangePresent = function (req, res) {
    var id = req.params.gameid, userId = req.body.userId, statsField;
    var present = req.body.present;
    Game.findOneAndUpdate(
        {"_id": id, "agreedUser.user": userId},
        {
            "$set": {
                "agreedUser.$.present": present
            }
        },
        function (err, game) {
            if (err) {
                sendJSONresponse(res, 400, err);
            } else {
                if (game.tournament && present) {
                    statsField = "attendedTournaments";
                } else if (game.tournament && !present) {
                    statsField = "missedTournaments";
                } else if (!game.tournament && present) {
                    statsField = "attendedTrainings";
                } else if (!game.tournament && !present) {
                    statsField = "missedTrainings";
                }
                updateStats(userId, statsField, function (err, user) {
                    if (err) {
                        sendJSONresponse(res, 400, err);
                    } else {
                        sendJSONresponse(res, 201, game);
                    }

                });
            }
        }
    );
};

module.exports.gameChangeTeam = function (req, res) {
    var id = req.params.gameid;
    var team1 = req.body.team1.user, team2 = req.body.team2.user;
    var tactic1 = req.body.team1.tactic, tactic2 = req.body.team2.tactic;
    var update = {"team1.user": team1, "team2.user": team2,
        "team1.tactic": tactic1, "team2.tactic": tactic2};

    Game.findByIdAndUpdate(id,
        { $set: update},
        { new: true },
        function (err, game) {
            if (err) {
                sendJSONresponse(res, 400, err);
            } else {
                sendJSONresponse(res, 201, game);
            }
        }
    );
};

module.exports.gameChangeOutcome = function (req, res) {
    var id = req.params.gameid, outcome = req.body.outcome, statsField;
    Game.findByIdAndUpdate(id,
        { $set: { won: outcome }},
        { new: true },
        function (err, game) {
            if (err) {
                sendJSONresponse(res, 400, err);
            } else {
                if (outcome) {
                    statsField = "tournamentsWon";
                } else {
                    statsField = "tournamentsLost";
                }
                updateStats(getAgreedUserIds(game.agreedUser), statsField, function (err, user) {
                    if (err) {
                        sendJSONresponse(res, 400, err);
                    } else {
                        sendJSONresponse(res, 201, game);
                    }
                });
            }
        }
    );
};

module.exports.gamesAddComment = function (req, res) {
    var id = req.params.gameid;
    var comment = {
        user: req.body.user,
        feedbackText: req.body.comment
    };
    if (id) {
        Game.findByIdAndUpdate(id,
            {$push: {"feedback": comment}},
            {safe: true, upsert: true},
            function (err, game) {
                if (err) {
                    sendJSONresponse(res, 400, err);
                } else {
                    sendJSONresponse(res, 201, game);
                }
            }
        );
    } else {
        sendJSONresponse(res, 404, {
            message: "no gameid in request"
        });
    }
};

module.exports.gamesDeleteOne = function (req, res) {
    var id = req.params.gameid;
    if (id) {
        Game
            .findByIdAndRemove(id)
            .exec(function (err, game) {
                if (err) {
                    sendJSONresponse(res, 404, err);
                    return;
                }
                sendJSONresponse(res, 204, game);
            });
    } else {
        sendJSONresponse(res, 404, {
            message: "no gameid in request"
        });
    }
};

var updateStats = function (userId, field, callback) {
    var path = {};
    path["stats." + field] = 1;
    if (!(userId instanceof Array)) {
        userId = [userId];
    }
    User.update(
        {"_id":{ $in: userId } },
        {$inc: path},
        {multi: true},
        function (err, user) {
            if (err) {
                callback(err);
            } else {
                callback(null, user);
            }
        }
    );
};

var getAgreedUserIds = function(agreedUsers) {
    var i, ids = [];
    for (i=0; i<agreedUsers.length; i++) {
        ids.push(agreedUsers[i].user);
    }
    return ids;
};

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};