/*global $, Chart, statsData */
var ctxTraining = $("#trainingChart");
var ctxTournament = $("#tournamentChart");
var ctxTournamentOutcome = $("#tournamentOutcomeChart");

var trainingData = {
    labels: ["Besuchte Trainings", "Verpasste Trainings"],
    datasets: [
        {
            data: [statsData.attendedTrainings,statsData.missedTrainings],
            backgroundColor: [
                "#4BC0C0",
                "#FF6384"
            ]
        }]
};
var tournamentData = {
    labels: ["Besuchte Turniere", "Verpasste Turniere"],
    datasets: [
        {
            data: [statsData.attendedTournaments,statsData.missedTournaments],
            backgroundColor: [
                "#4BC0C0",
                "#FF6384"
            ]
        }]
};
var tournamentOutcomeData = {
    labels: ["Gewonnene Turniere", "Verlorene Turniere"],
    datasets: [
        {
            data: [statsData.tournamentsWon,statsData.tournamentsLost],
            backgroundColor: [
                "#4BC0C0",
                "#FF6384"
            ]
        }]
};

var options = {
    legend: {
        display: true,
        labels: {
            fontColor: "rgb(255, 255, 255)"
        }
    }
};

var trainingChart = new Chart(ctxTraining, {
    type: "pie",
    data: trainingData,
    options: options
});
var tournamentChart = new Chart(ctxTournament, {
    type: "pie",
    data: tournamentData,
    options: options
});
var tournamentOutcomeChart = new Chart(ctxTournamentOutcome, {
    type: "pie",
    data: tournamentOutcomeData,
    options: options
});