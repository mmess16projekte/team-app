/* global Tactic*/
Tactic.TacticModel = function(options){
    var that={},
        counter=0,
        steps=[],
        currStep=0;
    function init(){
        return that;
    }
    function getStep(){
        return currStep;
    }
    function setStep(step){
        currStep=step;
    }
    //prevent from showing more steps than actually created
    function IsNotOutOfRange(){
        if (currStep<steps.length){
            return true;
        }
        return false;
    }
    //save object data of current step
    function saveAnimation(objects){

        var i,object={};
        var savedObjects=[];
        for(i=0;i<objects.length;i++){
            object={
                top:objects[i].top,
                left:objects[i].left,
                id:objects[i].id
            };
            savedObjects.push(object);
        }
        steps.push(savedObjects);

    }
    //convert into fabric.js animate command
    function getAnimationString(int){
        if(int<0){
            return "-="+Math.abs(int);
        }else{
            return"+="+int;
        }
    }
    //get objects from step before for animation
    function getSavedObjects(step){
        if(step<=0){
            return steps[0];
        }
        return steps[step-1];
    }

    //create animation objects, that are going to be animated with fabric.js in TacticView.
    //the animation path is calculated by the difference between the coordinates between the current and the step before
    function calculateAnimation(from,to){
        var numObjects,i,left,top,id,animationObject,animationObjects=[];
        if(steps[to]==undefined||steps[from]==undefined){
            return null;
        }
        numObjects=steps[to].length;

        for(i=0;i<numObjects;i++){
            left=steps[to][i].left-steps[from][i].left;
            top=steps[to][i].top-steps[from][i].top;
            id=steps[to][i].id;
            animationObject={
                left:getAnimationString(left),
                top:getAnimationString(top),
                id:id
            };
            animationObjects.push(animationObject);
        }
        return animationObjects;

    }
    function getAnimationData(){
        return steps;
    }
    function setAnimationData(data) {
        steps = data;
    }
    //the id is a simple int for every added object on the canvas
    function getId(){
        counter++;
        return ""+counter;

    }
    that.getSavedObjects=getSavedObjects;
    that.calculateAnimation=calculateAnimation;
    that.getId=getId;
    that.saveAnimation=saveAnimation;
    that.init = init;
    that.getStep=getStep;
    that.setStep=setStep;
    that.IsNotOutOfRange=IsNotOutOfRange;
    that.getAnimationData=getAnimationData;
    that.setAnimationData=setAnimationData;
    return that;
};
