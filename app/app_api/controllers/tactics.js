/* global require, module */
var mongoose = require("mongoose");
var Tactic = mongoose.model("Tactic");

module.exports.tacticList = function (req, res) {
    Tactic.find({})
        .exec(function (err, tactics) {
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                sendJSONresponse(res, 200, tactics);
            }
        });
};

module.exports.tacticsReadOne = function (req, res) {
    var id = req.params.tacticid;
    if (id) {
        Tactic
            .findById(id)
            .exec(function (err, tactic) {
                if (!tactic) {
                    sendJSONresponse(res, 404, {
                        message: "tacticid not found"
                    });
                    return;
                } else if (err) {
                    sendJSONresponse(res, 404, err);
                    return;
                }
                sendJSONresponse(res, 200, tactic);
            });
    } else {
        sendJSONresponse(res, 404, {
            message: "no tacticid in request"
        });
    }
};

module.exports.tacticCreate = function (req, res) {
    Tactic.create({
        name: req.body.name,
        data: req.body.data
    }, function (err, tactic) {
        if (err) {
            sendJSONresponse(res, 400, err);
        } else {
            sendJSONresponse(res, 201, tactic);
        }
    });
};
var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};
