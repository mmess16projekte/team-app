/* global Tactic,EventPublisher*/
Tactic.TacticController = function(options){
    var that = new EventPublisher(),
        canvasContainer,
        saveCommentButton,
        animationButton,
        forwardButton,
        backButton,
        saveTacticButton,
        loadTacticButton,
        fields;


    function init(){
        saveTacticButton=options.saveTacticButton;
        loadTacticButton = options.loadTacticButton;
        fields=options.fields;
        canvasContainer = options.canvasContainer;
        animationButton = options.animationButton;
        saveCommentButton = options.saveCommentButton;
        forwardButton=options.forwardButton;
        backButton=options.backButton;
        saveTacticButton.addEventListener("click",onSaveTacticButtonClicked);
        loadTacticButton.addEventListener("click",onLoadTacticButtonClicked);
        canvasContainer.addEventListener("dragenter", handleDragEnter, false);
        canvasContainer.addEventListener("dragover", handleDragOver, false);
        canvasContainer.addEventListener("dragleave", handleDragLeave, false);
        canvasContainer.addEventListener("drop", handleDrop, false);
        animationButton.addEventListener("click",onAnimationButtonClicked);
        animationButton.disabled=true;
        saveCommentButton.addEventListener("click",onSaveButtonClicked);
        forwardButton.addEventListener("click",onForwardButtonClicked);
        backButton.addEventListener("click",onBackButtonClicked);
        [].forEach.call(fields, function (img) {
            img.addEventListener("click", onFieldClicked);
        });
        return that;
    }
    function onSaveTacticButtonClicked(){
        that.notifyAll("saveTactic");
    }
    function onLoadTacticButtonClicked(){
        that.notifyAll("loadTactic");
    }
    function onFieldClicked(e){
        that.notifyAll("fieldClicked",{img:e.target});
    }
    function onObjectSaveButtonClicked(){
        that.notifyAll("objectSave");
    }

    function setOnClickListener(button){
        button.addEventListener("click",onObjectSaveButtonClicked);
    }


    function onForwardButtonClicked(){
        that.notifyAll("stepForward");
        animationButton.disabled=false;

    }
    function onBackButtonClicked(){
        that.notifyAll("stepBack");
        animationButton.disabled=false;
    }


    function onSaveButtonClicked(){
        that.notifyAll("saveAnimation");
    }
    function onAnimationButtonClicked(){
        that.notifyAll("animationStart");
        animationButton.disabled=true;

    }

    function handleDragOver(e) {
        if (e.preventDefault) {
            e.preventDefault(); // Necessary. Allows us to drop.
        }

        e.dataTransfer.dropEffect ="copy"; // See the section on the DataTransfer object.
        // NOTE: comment above refers to the article (see top) -natchiketa

        return false;
    }

    function handleDragEnter() {
        // this / e.target is the current hover target.
        this.classList.add("over");
    }

    function handleDragLeave() {
        this.classList.remove("over"); // this / e.target is previous target element.
    }

    function handleDrop(e) {
        // this / e.target is current target element.

        if (e.stopPropagation) {
            e.stopPropagation(); // stops the browser from redirecting.
        }

        that.notifyAll("imageDropped",{x:e.layerX,y:e.layerY});

        return false;
    }
    that.init=init;
    that.setOnClickListener=setOnClickListener;
    return that;
};
