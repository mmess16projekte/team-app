/* global require */
var mongoose = require("mongoose");

var feedbackSchema = new mongoose.Schema({
    user: {type: String, required: true},
    feedbackText: {type: String, required: true},
    createdOn: {
        type: Date,
        "default": Date.now
    }
});

var gameSchema = new mongoose.Schema({
    location: {type: String, required: true},
    date: {
        type: Date,
        "default": Date.now,
        required: true
    },
    comment: String,
    tournament: {
        type: Boolean,
        required: true
    },
    mode: String,
    cost: String,
    agreedUser: [{
        user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
        present: Boolean
    }],
    disagreedUser: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
    team1: {
        user: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
        tactic: { type: mongoose.Schema.Types.ObjectId, ref: "Tactic" }
    },
    team2: {
        user: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
        tactic: { type: mongoose.Schema.Types.ObjectId, ref: "Tactic" }
    },
    won: Boolean,
    feedback: [feedbackSchema]
});

mongoose.model("Game", gameSchema);