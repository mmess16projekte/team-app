/* global require */
var mongoose = require("mongoose");

var tacticDetailSchema = new mongoose.Schema({
    id: {type: String, required: true},
    left: {type: Number, required: true},
    top: {type: Number, required: true}
});

var tacticSchema = new mongoose.Schema({
    name: {type: String, required: true},
    data: [[tacticDetailSchema]]
});

mongoose.model("Tactic", tacticSchema);