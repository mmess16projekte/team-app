/* global require, module */
var passport = require("passport");
var mongoose = require("mongoose");
var User = mongoose.model("User");

/* GET "login" page */
module.exports.login = function(req, res){
    renderLogin(req, res);
};

/* GET "signup" page */
module.exports.signup = function(req, res){
    renderSignup(req, res);
};

var renderLogin = function (req, res) {
    res.render("login-form", {
        title: "Team-App Login",
        pageHeader: "Bitte Benutzerdaten eingeben",
        error: req.query.err
    });
};

var renderSignup = function (req, res) {
    res.render("signup-form", {
        title: "Team-App Signup",
        pageHeader: "Bitte Benutzerdaten eingeben",
        error: req.query.err
    });
};

module.exports.doSignup = function(req, res) {
    var isTrainer, user;
    if(!req.body.name || !req.body.email || !req.body.password) {
        res.redirect("/signup?err=true");
        return;
    }
    if (req.body.isTrainer === "true") {
        isTrainer = true;
    } else {
        isTrainer = false;
    }
    user = new User();

    user.name = req.body.name;
    user.email = req.body.email;
    user.trainer = isTrainer;

    user.setPassword(req.body.password);

    user.save(function(err) {
        if (err) {
            res.redirect("/signup");
        } else {
            passport.authenticate("local")(req, res, function () {
                res.redirect("/gameslist");
            });
        }
    });
};

module.exports.doLogin = function(req, res) {
    passport.authenticate("local", {
        successRedirect: "/gameslist",
        failureRedirect: "/login?err=true",
        failureFlash: true
    })(req, res);
};

module.exports.userList = function(req, res) {
    var sort = "-" + req.query.sort;
    User.find({})
        .select("name stats")
        .sort(sort)
        .exec(function (err, users) {
            if (err) {
                res.status(404);
                res.json(err);
            } else {
                res.status(200);
                res.json(users);
            }
        });
};

module.exports.logout = function (req, res) {
    req.logout();
    res.redirect("/");
};