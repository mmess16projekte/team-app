/* global require, module */
var express = require("express");
var router = express.Router();

var ctrlGames = require("../controllers/games");
var ctrlTactics = require("../controllers/tactics");

router.get("/games", ctrlGames.gamesList);
router.post("/games", ctrlGames.gamesCreate);
router.get("/games/:gameid", ctrlGames.gamesReadOne);
router.post("/games/:gameid/addcomment", ctrlGames.gamesAddComment);
router.put("/games/:gameid/changestatus", ctrlGames.gameChangeStatus);
router.put("/games/:gameid/changepresent", ctrlGames.gameChangePresent);
router.put("/games/:gameid/changeteam", ctrlGames.gameChangeTeam);
router.put("/games/:gameid/changeoutcome", ctrlGames.gameChangeOutcome);
router.delete("/games/:gameid", ctrlGames.gamesDeleteOne);

// tactics
router.get("/tactics", ctrlTactics.tacticList);
router.get("/tactics/:tacticid", ctrlTactics.tacticsReadOne);
router.post("/tactics", ctrlTactics.tacticCreate);

module.exports = router;