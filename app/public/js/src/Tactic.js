/* global $,Tactic,fabric*/
var Tactic = Tactic || {};
Tactic.tacticStart = (function() {
    "use strict";
    var that = {},
        tacticController,
        tacticModel,
        tacticView,images;
    function init(){
        initTacticController();
        initTacticModel();

    }
    function initTacticModel(){
        tacticModel = (new Tactic.TacticModel({
        })).init();
    }
    function initTacticController(){
        var saveTacticButton =document.querySelector("#tactic-save-button");
        var loadTacticButton = document.getElementById("load-tactic-button");
        var saveCommentButton= document.querySelector("#comment-save-button");
        var animationButton =document.getElementById("animation-button");
        var forwardButton =document.getElementById("forward-button");
        var backButton =document.getElementById("back-button");
        var canvasContainer = document.querySelector("#canvas-container");
        var menu= document.querySelector("#context-menu");
        var fields= document.querySelectorAll("#field-chooser img");
        tacticController = (new Tactic.TacticController({
            saveTacticButton:saveTacticButton,
            loadTacticButton: loadTacticButton,
            animationButton: animationButton,
            canvasContainer:canvasContainer,
            forwardButton:forwardButton,
            backButton:backButton,
            saveCommentButton:saveCommentButton,
            menu:menu,
            fields:fields
        })).init();
        tacticController.addEventListener("imageDropped",onImageDropped);
        tacticController.addEventListener("animationStart",onAnimationStart);
        tacticController.addEventListener("saveAnimation",onAnimationSaved);
        tacticController.addEventListener("stepForward",onStepForward);
        tacticController.addEventListener("stepBack",onStepBack);
        tacticController.addEventListener("fieldClicked",onFieldClicked);
        tacticController.addEventListener("saveTactic",onSaveTactic);
        tacticController.addEventListener("loadTactic",onLoadTactic);
        //add Eventlisteners for drag&drop images
        //http://jsfiddle.net/Ahammadalipk/w8kkc/185/
        images = document.querySelectorAll("#images img");
        [].forEach.call(images, function (img) {
            img.addEventListener("dragstart", handleDragStart, false);
            img.addEventListener("dragend", handleDragEnd, false);
        });
    }

    function onLoadTactic() {
        var xhttp = new XMLHttpRequest();
        var id = $("#load-tactic option:selected").val();
        var path = "/api/tactics/" + id;
        var response;

        xhttp.open("GET", path, true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.addEventListener("load", function (event) {
            if (xhttp.status === 200) {
                response = JSON.parse(xhttp.responseText);
                response = response.data;

                tacticModel.setAnimationData(response);
                tacticView.updateStepLabel(response.length-1);
                tacticModel.setStep(response.length-1);
                tacticView.resetObjects(tacticModel.getSavedObjects(response.length-1));

            }
        });
        xhttp.send();
    }

    function onSaveTactic() {
        var xhttp = new XMLHttpRequest();
        var data, path = "/api/tactics/";
        var name = $("#tactic-name").val();
        var saveObject= tacticModel.getAnimationData();
        
        data = {
            name: name,
            data: saveObject
        } ;
        xhttp.open("POST", path, true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify(data));
        $("#tactic-name").val("");

    }
    //show tactic field, after user has chosen
    function onFieldClicked(e){
        initTacticView(e.data.img.src);
    }

    function onStepForward(){
        var step=tacticModel.getStep();
        if(tacticModel.IsNotOutOfRange()){
            tacticView.updateStepLabel(step+1);
            tacticModel.setStep(step+1);
            tacticView.resetObjects(tacticModel.getSavedObjects(step+1));
        }


    }
    function onStepBack(){
        var step=tacticModel.getStep();
        if(step!=0){
            tacticView.updateStepLabel(step-1);
            tacticModel.setStep(step-1);
            tacticView.resetObjects(tacticModel.getSavedObjects(step-1));
        }
    }


    function onAnimationSaved(){
        var step=tacticModel.getStep();
        tacticModel.saveAnimation(tacticView.getObjects());
        tacticView.resetObjects(tacticModel.getSavedObjects(step));


    }
    //handle drag&drop of animation objects
    function handleDragStart() {
        [].forEach.call(images, function (img) {
            img.classList.remove("img_dragging");
        });
        this.classList.add("img_dragging");
    }

    function handleDragEnd() {
        // this/e.target is the source node.
        [].forEach.call(images, function (img) {
            img.classList.remove("img_dragging");
        });
    }


//animate the current step of  tactic animation
    function onAnimationStart(){
        var step=tacticModel.getStep();

        var animationObjects=tacticModel.calculateAnimation(step-1,step),i;
        if(animationObjects==null){
            return;
        }
        for(i=0;i<animationObjects.length;i++){
            tacticView.showAnimation(animationObjects[i].id,animationObjects[i].left,animationObjects[i].top);
        }

    }



    function onImageDropped(e){
        var img = document.querySelector("#images img.img_dragging");
        var id=tacticModel.getId();
        tacticView.addImage(img,e.data,id);



    }

    function initTacticView(fieldBackground){
        var animationDiv = document.querySelector("#animation");
        var fieldDiv = document.querySelector("#field-chooser");
        var commendDiv=document.querySelector("#commandos");
        var managementDiv=document.querySelector("#management");
        var canvasContainer = document.getElementById("canvas-container");
        var stepLabel =document.querySelector("#step-label");
        var menu=document.querySelector(".context-menu");
        var canvas = new fabric.Canvas("field");
        var contextCanvas = new fabric.StaticCanvas("menu-img");
        tacticView = (new Tactic.TacticView({
            field:canvas,
            stepLabel:stepLabel,
            menu:menu,
            contextCanvas:contextCanvas,
            fieldBackground:fieldBackground,
            canvasContainer:canvasContainer
        })).init();
        fieldDiv.style.display="none";
        animationDiv.style.display="block";
        commendDiv.style.display="block";
        managementDiv.style.display="block";
        tacticView.addEventListener("menuActivated",onMenuActivated);
    }

//create menu to change number of player objects
    function onMenuActivated(){
        var button = document.querySelector("#object-save-button");
        tacticController.setOnClickListener(button);
        tacticController.addEventListener("objectSave",onObjectSave);
    }
//update new number of player object
    function onObjectSave(){
        tacticView.updateObject();


    }
    that.init = init;
    return that;
}());
