/* global $ */
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.getAttribute("name"));
}

function drop(ev) {
    ev.preventDefault();
    var id = ev.dataTransfer.getData("text");
    var newNode = document.getElementsByName(id)[0].cloneNode(true);
    newNode.id = id;
    //try to delete same node from both teams to prevent duplicates
    var node = document.getElementById(id);
    if (node) {
        node.parentNode.removeChild(node);
    }
    //prevent nested dropping
    if (ev.target.classList.contains("drop")) {
        ev.target.appendChild(newNode);
    }
}

$("#save-teams").click(function() {
    var gameid = $("#save-teams").attr("name");
    var path = "/gameslist/" + gameid + "/changeteam";
    var xhttp = new XMLHttpRequest();
    var team1 = getIdsFromTeam("team1"), team2 = getIdsFromTeam("team2");
    var tactic1 = $("#team1-tactic option:selected").val(), tactic2 = $("#team2-tactic option:selected").val();
    
    var data = {
        team1: {
            user: team1,
            tactic: tactic1
        },
        team2: {
            user: team2,
            tactic: tactic2
        }
    };
    if (team1.length || team2.length) {
        xhttp.open("POST", path, true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify(data));
        window.location.reload(true);
    }
});

function getIdsFromTeam(teamId) {
    var i, ids = [];
    var subNodes = document.getElementById(teamId).children;
    for (i = 0; i < subNodes.length; i++) {
        ids.push(subNodes[i].id);
    }
    return ids;
}